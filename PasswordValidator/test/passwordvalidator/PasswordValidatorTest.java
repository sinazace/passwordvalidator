package passwordvalidator;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * It is assumed that spaces shouldn't be valid characters.
 * @author Sina Lyon 991512857
 */
public class PasswordValidatorTest {
	
	// For uppercase method

    @Test
    public void testContainUppercaseBoundaryOut() {
        assertFalse("Doesn't contain Uppercase",PasswordValidator.containUppercase("janice1234"));
    }
    
    @Test
    public void testContainUppercaseBoundaryIn() {
        assertFalse("Doesn't contain Uppercase",PasswordValidator.containUppercase("janIce1234"));
    }
    
    @Test
    public void testContainUppercaseException() {
        assertFalse("Doesn't contain Uppercase",PasswordValidator.containUppercase("jan"));
    }
    
    @Test
    public void testContainUppercaseRegular() {
        assertFalse("Doesn't contain Uppercase", PasswordValidator.containUppercase("jaNiCE1234"));
    }
	
	
	
	
    @Test
    public void testHasValidDigitCountBoundaryOut() {
        assertFalse("Less than 2 characters found",PasswordValidator.hasValidDigitCount("hngasd3as"));
    }
    
    @Test
    public void testHasValidDigitCountBoundaryIn() {
        assertTrue("Less than 2 characters found",PasswordValidator.hasValidDigitCount("hngasd33as"));
    }
    
    @Test
    public void testHasValidDigitCountException() {
        assertFalse("Less than 2 characters found",PasswordValidator.hasValidDigitCount("hngasddfds"));
    }
    
    @Test
    public void testHasValidDigitCountRegular() {
        assertTrue("Less than 2 characters found", PasswordValidator.hasValidDigitCount("hngasd33412"));
    }
    
   @Test
   public void testIsValidLengthBoundaryOut() {
       assertFalse("Invalid password length", PasswordValidator.isValidLength("1234567"));
   }
//    
   @Test
   public void testIsValidLengthBoundaryIn() {
     assertTrue("Invalid password length", PasswordValidator.isValidLength("12345678"));
   }
//    
  @Test
  public void testIsValidLengthException() {
      assertFalse("Invalid password length", PasswordValidator.isValidLength("bobbyb"));
  }
//    
  @Test
  public void testIsValidLengthRegular() {
       assertTrue("Invalid password length", PasswordValidator.isValidLength("bobbybrown23"));
  }
   
//   @Test
//   public void testIsValidLengthExceptionNull() {
//       //fail("Invalid password length");
//       assertFalse("Invalid password length", PasswordValidator.isValidLength(null));
//  }
//   
   
   
//   @Test
//   public void testIsValidLengthExceptionSpace() {
//       //fail("Invalid password length");
//       assertFalse("Invalid password length", PasswordValidator.isValidLength("       "));
//  }
//   

//   
  
}
