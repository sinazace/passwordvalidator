package passwordvalidator;

/**
 *
 * @author Sina Lyon
 */
public class PasswordValidator {
    
    public static void main(String argsp[]) {
        
    }
    
    /**
     * Checking the password length as well as detecting spaces.
     */
    public static boolean isValidLength(String password) {
        // note that in Java, the && will check only the conditions to the right of it if the 1st condition is true.
        return password != null && !password.contains(" ") && password.length() >= 8;
    }
    
    public static boolean containLowercase(String password) {
    	return password.matches("(?=.*[a-z])");
    }
    
    public static boolean containUppercase(String password) {
    	return password.matches("(?=.*[A-Z])");
    }
    
    public static boolean hasValidDigitCount(String password) {
        int numberCount = 0;
        for (char digit : password.toCharArray()) {
            // checking number digits
            numberCount += digit == '0' 
                    || digit == '1' 
                    || digit == '2' 
                    || digit == '3' 
                    || digit =='4' 
                    || digit == '5' 
                    || digit == '6' 
                    || digit == '7' 
                    || digit == '8' 
                    || digit == '9' ? 1 : 0;
        }
        return numberCount >= 2;
    }
}
